# Date Field TZ Conversion in Custom Dimension



## The Issue
The field `${date_test.reporting_period_start_date}` is returning two different values when used as the field itself and when directly referenced in a custom dimension.

See [Looker Support Ticket 579308](https://help.looker.com/hc/en-us/requests/579308)

### Example Look from Shared URL:

```https://<REPLACE_WITH_OWN_URL>/explore/no_data_connection_model/date_test?fields=report_start,date_test.reporting_period_start_date,report_start_add_days&fill_fields=date_test.reporting_period_start_date&f[date_test.date_filter]=1+quarters+ago+for+1+quarters&sorts=date_test.reporting_period_start_date+desc&limit=500&column_limit=50&vis=%7B%7D&filter_config=%7B%22date_test.date_filter%22%3A%5B%7B%22type%22%3A%22past%22%2C%22values%22%3A%5B%7B%22constant%22%3A%221%22%2C%22unit%22%3A%22c_qu%22%7D%2C%7B%7D%5D%2C%22id%22%3A0%2C%22error%22%3Afalse%7D%5D%7D&dynamic_fields=%5B%7B%22category%22%3A%22dimension%22%2C%22expression%22%3A%22%24%7Bdate_test.reporting_period_start_date%7D%22%2C%22label%22%3A%22Report+Start%22%2C%22value_format%22%3Anull%2C%22value_format_name%22%3Anull%2C%22dimension%22%3A%22report_start%22%2C%22_kind_hint%22%3A%22dimension%22%2C%22_type_hint%22%3A%22date%22%7D%2C%7B%22category%22%3A%22dimension%22%2C%22expression%22%3A%22add_days%280%2C+%24%7Bdate_test.reporting_period_start_date%7D%29%22%2C%22label%22%3A%22Report+Start+-+add_days%22%2C%22value_format%22%3Anull%2C%22value_format_name%22%3Anull%2C%22dimension%22%3A%22report_start_add_days%22%2C%22_kind_hint%22%3A%22dimension%22%2C%22_type_hint%22%3A%22date%22%7D%5D&origin=share-expanded```
