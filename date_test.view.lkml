view: date_test {
  derived_table: {
    sql: SELECT CAST( '2023-05-01' AS DATE ) AS date ;;
  }

  dimension_group: start {
    alias: [base.start]
    label: "Project Start"
    description: "Project Start."
    type: time
    datatype: date
    timeframes: [raw, date, week, month, quarter, year]
    convert_tz: no
    sql: ${TABLE}.date ;;
  }

  filter: date_filter {
    alias: [
      base.date_filter
    ]
    label: "Reporting Period Filter"
    description: "([Project Exit Date] >= Reporting Period Filter start date OR is null)
    AND [Project Start Date] <= Reporting Period Filter end date.
    Requires two \"dates\" to set the filter. If an Absolute Date Filter is used, only one Reporting Period Date Filter will be populated.
    When the filter option is ' on or after' the date entered will be applied to the Reporting Period Date Filter end date and Reporting Period Date Filter start date will be Null.
    When the filter option is 'is before' the date entered will be applied to the Reporting Period Date Filter start date and Reporting Period Date Filter end date will be Null.
    If the filter option 'is any time' is used, both Reporting Period Date Filter dates will be Null."
    type: date
    sql: ${start_raw} < COALESCE({% date_end date_filter %}, now())
      AND (${start_raw} >= {% date_start date_filter %} OR ${start_raw} is NULL OR {% date_start date_filter %} IS NULL)
       ;;
  }

  dimension: reporting_period_start_date {
    alias: [base.reporting_period_start_date]
    description: "The Start Date in the 'Reporting Period Filter'"
    type: date
    sql: {% date_start date_filter %} ;;
  }
}
